package com.example.lab9_f.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab9_f.ExampleAdapter
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.R
import com.example.lab9_f.data.Flight
import com.example.lab9_f.data.Passenger
import kotlinx.android.synthetic.main.fragment_shoutbox.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class FlightsFragment : Fragment(), ExampleAdapter.OnItemClickListener{

    private lateinit var login:String
    private lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI
    private lateinit var loggedUser:Passenger
    private lateinit var flightDate:String
    private lateinit var returnDate:String
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_shoutbox, container, false)
        val baseUrl = "http://192.168.0.10:8080/api/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)
        loggedUser = arguments?.getSerializable("user") as Passenger
        val from = arguments?.getString("from")
        val to = arguments?.getString("to")
        flightDate = arguments?.getString("flight_date").toString()
        returnDate = arguments?.getString("return_date").toString()

        if (from != null) {
            if (to != null) {
                getFlights(from,to)
            }
        }
        return root
    }

    fun updateData(temp:Array<Flight>){
        recycler_view.adapter = ExampleAdapter(temp,this)
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.setHasFixedSize(true)
    }
    private fun getFlights(from:String,to:String){
        val call = jsonPlaceholderAPI.getSpecificFlights(from,to)
        call.enqueue(object : Callback<Array<Flight>> {
            override fun onResponse(
                    call: Call<Array<Flight>?>,
                    response: Response<Array<Flight>?>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                val eim = response.body()!!
                updateData(eim)
            }

            override fun onFailure(
                    call: Call<Array<Flight>?>,
                    t: Throwable
            ) {
                println(t.message)
            }
        })
    }

    override fun onItemClick(item: Flight, position: Int) {
            val bundle = Bundle()
            bundle.putSerializable("flight", item)
            bundle.putSerializable("user", loggedUser)
            bundle.putString("flight_date", flightDate)
            bundle.putString("return_date", returnDate)
            val fragment: Fragment =
                LuggageFragment()
            fragment.arguments = bundle
            val fragmentManager: FragmentManager? = fragmentManager
            fragmentManager?.beginTransaction()
                ?.replace(R.id.nav_host_fragment, fragment)
                ?.addToBackStack(null)?.commit()
    }
}