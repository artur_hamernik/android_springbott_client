package com.example.lab9_f.ui.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.view.*
import android.widget.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.R
import com.example.lab9_f.data.Flight
import com.example.lab9_f.data.Passenger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*


class AirportsFragment : Fragment(){

    private lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI
    private lateinit var inputFrom:EditText
    private lateinit var inputTo:EditText
    private lateinit var buttonFind: Button
    private lateinit var loggedUser:Passenger
    private lateinit var setFlightDateButton: Button
    private lateinit var flightDateText:TextView
    private lateinit var checkBox: CheckBox
    private lateinit var setReturnDateButton: Button
    private lateinit var returnDateText:TextView
    private lateinit var returnLabel:TextView
    private lateinit var flightDate:String
    private lateinit var returnDate:String
    private lateinit var flipCities: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_airports, container, false)
        loggedUser = arguments?.getSerializable("user") as Passenger
        inputFrom = root.findViewById(R.id.inputFrom)
        inputTo = root.findViewById(R.id.inputTo)
        buttonFind = root.findViewById(R.id.buttonFind)
        setFlightDateButton = root.findViewById(R.id.aButtonFlight)
        flightDateText = root.findViewById(R.id.aFlightDate)
        checkBox = root.findViewById(R.id.checkBox)
        returnDateText = root.findViewById(R.id.aFlightDate2)
        setReturnDateButton = root.findViewById(R.id.aButtonReturnFlight)
        returnLabel = root.findViewById(R.id.textView7)
        flipCities = root.findViewById(R.id.flipCities)

        val baseUrl = "http://192.168.0.10:8080/api/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()

        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)

        flipCities.setOnClickListener {
            val from = inputFrom.text.toString()
            val to = inputTo.text.toString()

            inputFrom.setText(to)
            inputTo.setText(from)
        }

        checkBox.setOnClickListener {
            if(returnDateText.isVisible){
                returnDateText.visibility = View.GONE
                setReturnDateButton.visibility = View.GONE
                returnLabel.visibility = View.GONE
            }
            else{
                returnDateText.visibility = View.VISIBLE
                setReturnDateButton.visibility = View.VISIBLE
                returnLabel.visibility = View.VISIBLE
            }
        }

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        setFlightDateButton.setOnClickListener {
            context?.let { it1 ->
                DatePickerDialog(it1, DatePickerDialog.OnDateSetListener{ DatePicker, mYear, mMonth, mDay->
                    flightDateText.setText(""+mDay+"/"+(mMonth+1)+"/"+mYear)
                },year,month,day)
            }?.show()
        }
        setReturnDateButton.setOnClickListener {
            context?.let { it1 ->
                DatePickerDialog(it1, DatePickerDialog.OnDateSetListener{ DatePicker, mYear, mMonth, mDay->
                    returnDateText.setText(""+mDay+"/"+(mMonth+1)+"/"+mYear)
                },year,month,day)
            }?.show()
        }

        buttonFind.setOnClickListener {
            flightDate = flightDateText.text.toString()
            returnDate = returnDateText.text.toString()
            if(inputFrom.text.toString()== "" || inputTo.text.toString() == ""){
                Toast.makeText(context, "Flight destinations can't be empty!", Toast.LENGTH_SHORT).show()
            }
            else {
                if (flightDate != "") {
                    val format = SimpleDateFormat("dd/MM/yyyy")
                    val date1: Date = format.parse(flightDate)
                    val date2: Date = format.parse("" + day + "/" + month + "/" + year)
                    if (date1 <= date2) {
                        Toast.makeText(context, "Flight date is invalid!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        if (returnDate != "") {
                            val date3: Date = format.parse(returnDate)
                            if (date1 < date3) {
                                val fromm = inputFrom.text.toString().capitalize()
                                val too = inputTo.text.toString().capitalize()
                                val from = SpannableStringBuilder(fromm)
                                val to = SpannableStringBuilder(too)
                                getFlights(from, to)
                            } else {
                                Toast.makeText(
                                    context,
                                    "Return date is invalid!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            val fromm = inputFrom.text.toString().capitalize()
                            val too = inputTo.text.toString().capitalize()
                            val from = SpannableStringBuilder(fromm)
                            val to = SpannableStringBuilder(too)
                            getFlights(from, to)
                        }
                    }
                } else {
                    Toast.makeText(context, "Flight date is invalid!", Toast.LENGTH_SHORT).show()
                }
            }

            }
            return root

    }



    private fun getFlights(from: Editable, to: Editable){
        val call = jsonPlaceholderAPI.getSFlights(from,to)
        call.enqueue(object : Callback<Array<Flight>> {
            override fun onResponse(
                call: Call<Array<Flight>?>,
                response: Response<Array<Flight>?>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                val eim = response.body()!!
                if(eim.isNotEmpty()) {
                    val bundle = Bundle()
                    bundle.putString("from", from.toString())
                    bundle.putString("to", to.toString())
                    bundle.putString("flight_date", flightDate)
                    bundle.putString("return_date", returnDate)
                    bundle.putSerializable("user",loggedUser)
                    val fragment: Fragment =
                        FlightsFragment()
                    fragment.arguments = bundle
                    val fragmentManager: FragmentManager? = fragmentManager
                    fragmentManager?.beginTransaction()
                        ?.replace(R.id.nav_host_fragment, fragment)
                        ?.addToBackStack(null)?.commit()
                }
                else{
                    Toast.makeText(context, "There is no connection between these cities!", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(
                call: Call<Array<Flight>?>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }
}


