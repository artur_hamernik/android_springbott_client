package com.example.lab9_f.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.lab9_f.R
import com.example.lab9_f.data.Passenger
import java.util.zip.Inflater

class PanelFragment: Fragment()

{
 private lateinit var loggedUser:Passenger
 override fun onCreateView(
  inflater: LayoutInflater,
  container: ViewGroup?,
  savedInstanceState: Bundle?
 ): View? {
      val root = inflater.inflate(R.layout.fragment_panel, container, false)
      val bookNewFlight = root.findViewById<Button>(R.id.newFlight)
      val yourFlights = root.findViewById<Button>(R.id.yourFlights)
      loggedUser = arguments?.getSerializable("user") as Passenger
      bookNewFlight.setOnClickListener {

        val bundle = Bundle()
        bundle.putSerializable("user", loggedUser)
        val fragment: Fragment =
         AirportsFragment()
        fragment.arguments = bundle
        val fragmentManager: FragmentManager? = fragmentManager
        fragmentManager?.beginTransaction()
         ?.replace(R.id.nav_host_fragment, fragment)
         ?.addToBackStack(null)?.commit()

      }

      yourFlights.setOnClickListener {

       val bundle = Bundle()
       bundle.putSerializable("user", loggedUser)
       val fragment: Fragment =
        ConfirmFragment()
       fragment.arguments = bundle
       val fragmentManager: FragmentManager? = fragmentManager
       fragmentManager?.beginTransaction()
        ?.replace(R.id.nav_host_fragment, fragment)
        ?.addToBackStack(null)?.commit()

      }

  return root
 }


 }







