package com.example.lab9_f.ui.fragments

import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.PassFlyAdapter
import com.example.lab9_f.R
import com.example.lab9_f.data.Flight
import com.example.lab9_f.data.Luggage
import com.example.lab9_f.data.Passenger
import com.example.lab9_f.data.PassengerFlights
import kotlinx.android.synthetic.main.fragment_confirm.*
import kotlinx.android.synthetic.main.fragment_shoutbox.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ConfirmFragment : Fragment(), PassFlyAdapter.OnItemClickListener{


    private lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI
    private lateinit var loggedUser:Passenger
    private lateinit var backToPanel: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_confirm, container, false)
        val baseUrl = "http://192.168.0.10:8080/api/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)
        loggedUser = arguments?.getSerializable("user") as Passenger
        backToPanel = root.findViewById(R.id.backToPanelBtn)

        backToPanel.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("user", loggedUser)

            val fragment: Fragment =
                PanelFragment()
            fragment.arguments = bundle

            val fragmentManager: FragmentManager? = fragmentManager
            fragmentManager?.beginTransaction()?.replace(R.id.nav_host_fragment, fragment)?.
                addToBackStack(null)?.commit()
        }

        getPassFly(loggedUser)

        return root
    }


    fun updateData(temp:Array<PassengerFlights>){
        recycler_view_2.adapter = PassFlyAdapter(temp,this)
        recycler_view_2.layoutManager = LinearLayoutManager(context)
        recycler_view_2.setHasFixedSize(true)
    }
    private fun getPassFly(passenger: Passenger){
        val call = jsonPlaceholderAPI.getPassFly(passenger.id_customer)
        call.enqueue(object : Callback<Array<PassengerFlights>> {
            override fun onResponse(
                call: Call<Array<PassengerFlights>?>,
                response: Response<Array<PassengerFlights>?>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                val eim = response.body()!!
                updateData(eim)
            }
            override fun onFailure(
                call: Call<Array<PassengerFlights>?>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }


    override fun onItemClick(item: PassengerFlights, position: Int) {
        if(item.payment?.status == "Unpaid") {
            val bundle = Bundle()
            bundle.putSerializable("user", loggedUser)
            bundle.putSerializable("flight", item.flight)
            bundle.putSerializable("luggage", item.luggage)
            bundle.putSerializable("flight_pass", item)
            bundle.putString("flight_date", item.departureDate)
            bundle.putString("return_date", item.arrivalDate)
            val fragment: Fragment =
                PaymentFragment()
            fragment.arguments = bundle
            val fragmentManager: FragmentManager? = fragmentManager
            fragmentManager?.beginTransaction()
                ?.replace(R.id.nav_host_fragment, fragment)
                ?.addToBackStack(null)?.commit()
        }
        else{
            Toast.makeText(context,"This flight was paid!",Toast.LENGTH_SHORT).show()
        }
    }
}