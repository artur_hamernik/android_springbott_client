package com.example.lab9_f.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.R
import com.example.lab9_f.data.Passenger
import kotlinx.android.synthetic.main.fragment_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class LoginFragment : Fragment() {
    private lateinit var editLogin:EditText
    private lateinit var password:EditText
    private lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI
    private var userLogin:Passenger = Passenger()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {


        val root = inflater.inflate(R.layout.fragment_login, container, false)
        val button = root.findViewById<Button>(R.id.button)
        val bRegister = root.findViewById<Button>(R.id.logRegister)
        val baseUrl = "http://192.168.0.10:8080/api/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)

        setHasOptionsMenu(false)

        editLogin = root.findViewById(R.id.editLogin)
        password = root.findViewById(R.id.editPassword)
        readdata()
        button.setOnClickListener {
            val login =  editLogin.text.toString()
            val password = password.text.toString()
            getUser(login,password)
        }
        bRegister.setOnClickListener {
            val fragment: Fragment =
                RegistrationFragment()
            val fragmentManager: FragmentManager? = fragmentManager
            fragmentManager?.beginTransaction()
                ?.replace(R.id.nav_host_fragment, fragment)
                ?.addToBackStack(null)?.commit()
        }
        return root
    }


    private fun savedata(){
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(getString(R.string.saved_login), editLogin.text.toString())
            putString(getString(R.string.saved_password), editPassword.text.toString())
            commit()
        }
    }
    private fun readdata(){
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val defaultValue = resources.getString(R.string.login)
        editLogin.setText(sharedPref.getString(getString(R.string.saved_login), defaultValue))
        password.setText(sharedPref.getString(resources.getString(R.string.saved_password),null))
    }
    private fun getUser(login: String, password: String){
        val call = jsonPlaceholderAPI.getUser(login,password)
        call.enqueue(object : Callback<Passenger> {
            override fun onResponse(
                call: Call<Passenger>,
                response: Response<Passenger>
            ) {
                if (!response.isSuccessful) {
                    Toast.makeText(context,"User with this login or password doesn't exist!",Toast.LENGTH_SHORT).show()
                    println("Code: " + response.code())
                    return
                }
                userLogin = response.body()!!
                println("Name: " + userLogin.name)
                println("Surname: " + userLogin.surname)

                savedata()

                val bundle = Bundle()
                bundle.putSerializable("user", userLogin)
                val fragment: Fragment =
                    PanelFragment()
                fragment.arguments = bundle
                val fragmentManager: FragmentManager? = fragmentManager
                fragmentManager?.beginTransaction()
                    ?.replace(R.id.nav_host_fragment, fragment)
                    ?.addToBackStack(null)?.commit()
            }

            override fun onFailure(
                call: Call<Passenger>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }
}