package com.example.lab9_f.ui.fragments

import android.app.ActionBar
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.R
import com.example.lab9_f.data.Document
import com.example.lab9_f.data.Passenger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class RegistrationFragment : Fragment() {

    private lateinit var regName:EditText
    private lateinit var regSurname:EditText
    private lateinit var regBirth:EditText
    private lateinit var regSex:EditText
    private lateinit var regSerial:EditText
    private lateinit var regType:EditText
    private lateinit var regExpir:EditText
    private lateinit var regCitizen:EditText
    private lateinit var regLogin:EditText
    private lateinit var regPassword:EditText
    private lateinit var register:Button
    private lateinit var datePickerBir: Button
    private lateinit var datePickerExpi: Button
    private lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI

    val user = Passenger()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_registration, container, false)
        val baseUrl = "http://192.168.0.10:8080/api/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)
        regName = root.findViewById(R.id.regName)
        regSurname = root.findViewById(R.id.regSurname)
        regBirth = root.findViewById(R.id.regBirth)
        regSex = root.findViewById(R.id.regSex)
        regSerial = root.findViewById(R.id.regSerial)
        regType = root.findViewById(R.id.regType)
        regExpir = root.findViewById(R.id.regExpir)
        regCitizen = root.findViewById(R.id.regCitizen)
        regLogin = root.findViewById(R.id.regLogin)
        regPassword = root.findViewById(R.id.regPassword)
        register = root.findViewById(R.id.regButton)
        datePickerBir = root.findViewById(R.id.date_picker_reg_bir)
        datePickerExpi = root.findViewById(R.id.date_picker_reg_expi)


        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        datePickerExpi.setOnClickListener {
            context?.let { it1 ->
                DatePickerDialog(it1, DatePickerDialog.OnDateSetListener{ DatePicker, mYear, mMonth, mDay->
                    regExpir.setText(""+mDay+"/"+(mMonth+1)+"/"+mYear)
                },year,month,day)
            }?.show()
        }
         datePickerBir.setOnClickListener {
             context?.let { it1 ->
                 DatePickerDialog(it1, DatePickerDialog.OnDateSetListener{ DatePicker, mYear, mMonth, mDay->
                     regBirth.setText(""+mDay+"/"+(mMonth+1)+"/"+mYear)
                 },year,month,day)
             }?.show()
         }

        register.setOnClickListener {
            val document = Document()
            document.document_serial = regSerial.text.toString()
            document.document_type = regType.text.toString()
            document.expiration_date = regExpir.text.toString()
            document.citizenship = regCitizen.text.toString()


            user.name = regName.text.toString()
            user.surname = regSurname.text.toString()
            user.birth_date = regBirth.text.toString()
            user.login = regLogin.text.toString()
            user.password = regPassword.text.toString()
            user.sex = regSex.text.toString()
            user.document = document

            getPassengers()

        }
        return root
    }
    private fun postPassenger(passenger: Passenger){
        val call = jsonPlaceholderAPI.postPassenger(passenger)

        call.enqueue(object : Callback<Passenger> {
            override fun onResponse(
                call: Call<Passenger>,
                response: Response<Passenger>
            ) {
                if (!response.isSuccessful) {
                    Toast.makeText(context, "Registration failed!", Toast.LENGTH_SHORT).show()
                    println("Code: " + response.code())
                    return
                }
                Toast.makeText(context,"Registration successful!!",Toast.LENGTH_SHORT).show()
            }
            override fun onFailure(
                call: Call<Passenger>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }
    private fun getPassengers(){
        val call = jsonPlaceholderAPI.getPassengers()
        call.enqueue(object : Callback<Array<Passenger>> {
            override fun onResponse(
                call: Call<Array<Passenger>>,
                response: Response<Array<Passenger>>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                val passengers = response.body()!!

                var flag = 0
                for(passenger in passengers){
                    if(passenger.login == user.login){
                        Toast.makeText(context,"Passenger with this login already exists!",Toast.LENGTH_SHORT).show()
                        flag = 1
                        break
                    }
                }

                if(flag == 0){
                    postPassenger(user)
                    val fragment: Fragment =
                        LoginFragment()
                    val fragmentManager: FragmentManager? = fragmentManager
                    fragmentManager?.beginTransaction()
                        ?.replace(R.id.nav_host_fragment, fragment)
                        ?.addToBackStack(null)?.commit()
                }
            }
            override fun onFailure(
                call: Call<Array<Passenger>>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }
}