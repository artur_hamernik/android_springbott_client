package com.example.lab9_f.ui.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.R
import com.example.lab9_f.data.*
import kotlinx.android.synthetic.main.layout_dialog.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class PaymentFragment : Fragment() {

    private lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI
    private lateinit var flight:Flight
    private lateinit var luggage: Luggage
    private lateinit var loggedUser:Passenger
    private lateinit var passFly:PassengerFlights
    private lateinit var flightDate:String
    private lateinit var returnDate:String
    private lateinit var payBlik:Button
    private lateinit var cost1:TextView
    private lateinit var cost2:TextView
    private lateinit var cost3:TextView
    private lateinit var cost4:TextView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_payment, container, false)
        val baseUrl = "http://192.168.0.10:8080/api/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)


        cost1 = root.findViewById(R.id.payF)
        cost2 = root.findViewById(R.id.payReturn)
        cost3 = root.findViewById(R.id.payLuggage)
        cost4 = root.findViewById(R.id.payTotal)
        payBlik = root.findViewById(R.id.payWithBlik)


        flight = arguments?.getSerializable("flight") as Flight
        luggage = arguments?.getSerializable("luggage") as Luggage
        loggedUser = arguments?.getSerializable("user") as Passenger
        flightDate = arguments?.getString("flight_date").toString()
        returnDate = arguments?.getString("return_date").toString()
        passFly = arguments?.getSerializable("flight_pass") as PassengerFlights

        val costF =  flight.price
        var costR = 0
        if(returnDate != ""){
            costR = flight.price
        }
        val costL = luggage.price
        val costT = costF + costR + costL;
        cost1.text = costF.toString() + " zł"
        cost2.text = costR.toString() + " zł"
        cost3.text = costL.toString() + " zł"
        cost4.text = costT.toString() + " zł"

        payBlik.setOnClickListener {
            val mDiaView = LayoutInflater.from(context).inflate(R.layout.layout_dialog,null)
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDiaView)
                .setTitle("Pay with BLIK")
            val mAlertDialog = mBuilder.show()
            mDiaView.payDialog.setOnClickListener {
                val blikCode = mDiaView.blikCodeDialog.text.toString()
                if(passFly.flight == null) {
                    if (blikCode.length < 6) {
                        Toast.makeText(context, "Blik invalid", Toast.LENGTH_SHORT).show()
                    } else {
                        mAlertDialog.dismiss()
                        val c = Calendar.getInstance()
                        val year = c.get(Calendar.YEAR)
                        val month = c.get(Calendar.MONTH)
                        val day = c.get(Calendar.DAY_OF_MONTH)

                        val payment = Payment()
                        payment.payment_type = "BLIK"
                        payment.pay_value = costT
                        payment.currency = "PLN"
                        payment.date = "" + day + "/" + (month + 1) + "/" + year
                        payment.status = "Paid"

                        val passengerFlights = PassengerFlights()
                        passengerFlights.departureDate = flightDate
                        passengerFlights.arrivalDate = ""
                        if (returnDate != "") {
                            passengerFlights.arrivalDate = returnDate
                        }
                        passengerFlights.flight = flight
                        passengerFlights.id_passenger = loggedUser.id_customer
                        passengerFlights.payment = payment
                        passengerFlights.luggage = luggage
                        postPassFly(passengerFlights)
                        Toast.makeText(context, "Transaction successful!", Toast.LENGTH_SHORT)
                            .show()
                        val bundle = Bundle()
                        bundle.putSerializable("flight_pass", PassengerFlights())
                        bundle.putSerializable("user", loggedUser)
                        val fragment: Fragment =
                            PanelFragment()
                        fragment.arguments = bundle
                        val fragmentManager: FragmentManager? = fragmentManager
                        fragmentManager?.beginTransaction()
                            ?.replace(R.id.nav_host_fragment, fragment)
                            ?.addToBackStack(null)?.commit()
                    }
                }
                else{
                    if (blikCode.length < 6) {
                        Toast.makeText(context, "Blik invalid", Toast.LENGTH_SHORT).show()
                    } else {
                        mAlertDialog.dismiss()
                        val c = Calendar.getInstance()
                        val year = c.get(Calendar.YEAR)
                        val month = c.get(Calendar.MONTH)
                        val day = c.get(Calendar.DAY_OF_MONTH)

                        val payment = Payment()
                        payment.payment_type = "BLIK"
                        payment.pay_value = costT
                        payment.currency = "PLN"
                        payment.date = "" + day + "/" + (month + 1) + "/" + year
                        payment.status = "Paid"
                        passFly.payment = payment
                        putPassFly(passFly)
                        val bundle = Bundle()
                        bundle.putSerializable("user", loggedUser)
                        val fragment: Fragment =
                            PanelFragment()
                        fragment.arguments = bundle
                        val fragmentManager: FragmentManager? = fragmentManager
                        fragmentManager?.beginTransaction()
                            ?.replace(R.id.nav_host_fragment, fragment)
                            ?.addToBackStack(null)?.commit()
                    }
                }
            }
            mDiaView.cancelDialog.setOnClickListener {
                mAlertDialog.dismiss()
                if(passFly.flight == null) {
                    val payment = Payment()
                    payment.payment_type = ""
                    payment.pay_value = costT
                    payment.currency = ""
                    payment.date = ""
                    payment.status = "Unpaid"

                    val passengerFlights = PassengerFlights()
                    passengerFlights.departureDate = flightDate
                    passengerFlights.arrivalDate = ""
                    if (returnDate != "") {
                        passengerFlights.arrivalDate = returnDate
                    }
                    passengerFlights.flight = flight
                    passengerFlights.id_passenger = loggedUser.id_customer
                    passengerFlights.payment = payment
                    passengerFlights.luggage = luggage
                    postPassFly(passengerFlights)
                    val bundle = Bundle()
                    bundle.putSerializable("user", loggedUser)
                    val fragment: Fragment =
                        PanelFragment()
                    fragment.arguments = bundle
                    val fragmentManager: FragmentManager? = fragmentManager
                    fragmentManager?.beginTransaction()
                        ?.replace(R.id.nav_host_fragment, fragment)
                        ?.addToBackStack(null)?.commit()
                }
                Toast.makeText(context, "Transaction canceled", Toast.LENGTH_SHORT).show()
            }
        }
        return root
    }

    private fun postPassFly(passengerFlights: PassengerFlights){
        val call = jsonPlaceholderAPI.postPassFly(passengerFlights)

        call.enqueue(object : Callback<PassengerFlights> {
            override fun onResponse(
                call: Call<PassengerFlights>,
                response: Response<PassengerFlights>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
            }
            override fun onFailure(
                call: Call<PassengerFlights>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }
    private fun putPassFly(passengerFlights: PassengerFlights){
        val call = jsonPlaceholderAPI.putPassFly(passengerFlights)
        call.enqueue(object : Callback<PassengerFlights> {
            override fun onResponse(
                call: Call<PassengerFlights>,
                response: Response<PassengerFlights>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
            }
            override fun onFailure(
                call: Call<PassengerFlights>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }
}