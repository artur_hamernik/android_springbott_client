package com.example.lab9_f.ui.fragments

import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.lab9_f.JsonPlaceholderAPI
import com.example.lab9_f.R
import com.example.lab9_f.data.Flight
import com.example.lab9_f.data.Luggage
import com.example.lab9_f.data.Passenger
import com.example.lab9_f.data.PassengerFlights
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class LuggageFragment : Fragment(){

    private lateinit var textAirline: TextView
    private lateinit var textFromGMT: TextView
    private lateinit var textFrom: TextView
    private lateinit var textTo: TextView
    private lateinit var textToGMT: TextView
    private lateinit var textDeparture: TextView
    private lateinit var textArrival: TextView
    private lateinit var flight: Flight
    private lateinit var jsonPlaceholderAPI:JsonPlaceholderAPI
    private lateinit var imageLug: ImageView
    private lateinit var imageLug15: ImageView
    private lateinit var imageLug23: ImageView
    private lateinit var imageLug26: ImageView
    private lateinit var loggedUser:Passenger
    private lateinit var flightDate:String
    private lateinit var returnDate:String



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_luggage, container, false)
        val baseUrl = "http://192.168.0.10:8080/api/"
        val retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)

        flight = arguments?.getSerializable("flight") as Flight
        loggedUser = arguments?.getSerializable("user") as Passenger
        flightDate = arguments?.getString("flight_date").toString()
        returnDate = arguments?.getString("return_date").toString()



        textAirline = root.findViewById(R.id.textAirlineD)
        textFrom = root.findViewById(R.id.textFromD)
        textTo = root.findViewById(R.id.textToD)
        textToGMT = root.findViewById(R.id.textArrivalGmtD)
        textFromGMT = root.findViewById(R.id.textDepartureGmtD)
        textDeparture = root.findViewById(R.id.textDepartureD)
        textArrival = root.findViewById(R.id.textArrivalD)

        imageLug = root.findViewById(R.id.imageLug)
        imageLug15 = root.findViewById(R.id.imageLug15)
        imageLug23 = root.findViewById(R.id.imageLug23)
        imageLug26 = root.findViewById(R.id.imageLug26)

        textAirline.text = flight.airline?.name
        textFrom.text = flight.from?.name
        textTo.text = flight.to?.name
        textToGMT.text = flight.to?.timezone
        textFromGMT.text = flight.from?.timezone
        textDeparture.text = flight.departure
        textArrival.text = flight.arrival



        imageLug.setOnClickListener {
            getLuggage(10)
        }
        imageLug15.setOnClickListener {
            getLuggage(15)
        }
        imageLug23.setOnClickListener {
            getLuggage(23)
        }
        imageLug26.setOnClickListener {
            getLuggage(26)
        }

        return root
    }



    fun goNextFragment(luggage: Luggage){
        val bundle = Bundle()
        bundle.putSerializable("flight_pass", PassengerFlights())
        bundle.putSerializable("flight", flight)
        bundle.putSerializable("luggage", luggage)
        bundle.putSerializable("user", loggedUser)
        bundle.putString("flight_date", flightDate)
        bundle.putString("return_date", returnDate)
        val fragment: Fragment =
            PaymentFragment()
        fragment.arguments = bundle
        val fragmentManager: FragmentManager? = fragmentManager
        fragmentManager?.beginTransaction()
            ?.replace(R.id.nav_host_fragment, fragment)
            ?.addToBackStack(null)?.commit()
    }
    private fun getLuggage(weight:Int){
        val call = jsonPlaceholderAPI.getLuggage(weight)
        call.enqueue(object : Callback<Luggage> {
            override fun onResponse(
                call: Call<Luggage>,
                response: Response<Luggage>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                val luggage = response.body()!!
                goNextFragment(luggage)
            }

            override fun onFailure(
                call: Call<Luggage>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }
}