package com.example.lab9_f

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lab9_f.data.Flight
import com.example.lab9_f.data.PassengerFlights
import kotlinx.android.synthetic.main.example_item.view.*
import kotlinx.android.synthetic.main.passfly_item.view.*

class PassFlyAdapter(private var exampleList: Array<PassengerFlights>, var clickListener: OnItemClickListener): RecyclerView.Adapter<PassFlyAdapter.ExampleViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.passfly_item,
            parent, false)

        return ExampleViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
        holder.initialize(exampleList[position],clickListener)

    }
    override fun getItemCount() = exampleList.size

    class ExampleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var textArrival: TextView = itemView.pfArr
        var textDeparture: TextView = itemView.pfDep
        var textFrom: TextView = itemView.pfFrom
        var textTo: TextView = itemView.pfTo
        var textAirline: TextView = itemView.pfAirline
        var textArrivalGmt: TextView = itemView.pfArrGmt
        var textDepartureGmt: TextView = itemView.pfDepGmt
        var textPrice: TextView = itemView.pfPrice
        var textPayStatus:TextView = itemView.pfPayStatus
        var textArrivalDate:TextView = itemView.pfArrDate
        var textDepartureDate:TextView = itemView.pfDepDate

        fun initialize(item: PassengerFlights, action: OnItemClickListener){
            textAirline.text = item.flight?.airline?.name
            textArrival.text = item.flight?.arrival
            textDeparture.text = item.flight?.departure
            textFrom.text = item.flight?.from?.name
            textTo.text = item.flight?.to?.name
            textArrivalGmt.text = item.flight?.to?.timezone
            textDepartureGmt.text = item.flight?.from?.timezone
            textPrice.text = item.flight?.price.toString()+" zł"
            textPayStatus.text = item.payment?.status
            textArrivalDate.text = item.arrivalDate
            textDepartureDate.text = item.departureDate
            itemView.setOnClickListener {
                action.onItemClick(item, adapterPosition)
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(item: PassengerFlights, position:Int)
    }
}