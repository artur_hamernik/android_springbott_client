package com.example.lab9_f

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lab9_f.data.Flight
import kotlinx.android.synthetic.main.example_item.view.*

class ExampleAdapter(private var exampleList: Array<Flight>, var clickListener: OnItemClickListener): RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExampleViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.example_item,
            parent, false)

        return ExampleViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: ExampleViewHolder, position: Int) {
        holder.initialize(exampleList[position],clickListener)

    }
    override fun getItemCount() = exampleList.size

    class ExampleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var textArrival: TextView = itemView.eiArr
        var textDeparture: TextView = itemView.eiDep
        var textFrom: TextView = itemView.eiFrom
        var textTo: TextView = itemView.eiTo
        var textAirline: TextView = itemView.eiAirline
        var textArrivalGmt: TextView = itemView.eiArrGmt
        var textDepartureGmt: TextView = itemView.eiDepGmt
        var textPrice: TextView = itemView.eiPrice

        fun initialize(item: Flight, action: OnItemClickListener){
            textAirline.text = item.airline!!.name
            textArrival.text = item.arrival
            textDeparture.text = item.departure
            textFrom.text = item.from!!.name
            textTo.text = item.to!!.name
            textArrivalGmt.text = item.to.timezone
            textDepartureGmt.text = item.from.timezone
            textPrice.text = item.price.toString()+" zł"
            itemView.setOnClickListener {
                action.onItemClick(item, adapterPosition)
            }
        }
    }


    interface OnItemClickListener{
        fun onItemClick(item: Flight, position:Int)
    }
}