package com.example.lab9_f

import android.text.Editable
import com.example.lab9_f.data.Flight
import com.example.lab9_f.data.Luggage
import com.example.lab9_f.data.Passenger
import com.example.lab9_f.data.PassengerFlights
import retrofit2.Call
import retrofit2.http.*

interface JsonPlaceholderAPI {

    @GET("passengers")
    fun getPassengers():Call<Array<Passenger>>

    @GET("flights")
    fun getFlights():Call<Array<Flight>>

    @GET("whereflights")
    fun getSpecificFlights(@Query("from") from:String,
                           @Query("to") to:String):Call<Array<Flight>>

    @GET("whereflights")
    fun getSFlights(@Query("from") from: Editable,
                    @Query("to") to: Editable): Call<Array<Flight>>

    @POST("postPassenger")
    fun postPassenger(@Body passenger: Passenger):Call<Passenger>

    @GET("user")
    fun getUser(@Query("login") login: String,
                @Query("password") password: String):Call<Passenger>

    @GET("luggage")
    fun getLuggage(@Query("weight") weight: Int):Call<Luggage>

    @POST("postpassfly")
    fun postPassFly(@Body passengerFlight: PassengerFlights):Call<PassengerFlights>

    @GET("passfly")
    fun getPassFly(@Query("id_passenger") id_passenger:Int):Call<Array<PassengerFlights>>

    @POST("putpassfly")
    fun putPassFly(@Body passengerFlight: PassengerFlights):Call<PassengerFlights>

}