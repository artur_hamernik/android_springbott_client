package com.example.lab9_f.data

import java.io.Serializable

class PassengerFlights :Serializable{
    var flight: Flight? = null
    var id_passenger = 0
    var arrivalDate: String? = null
    var departureDate: String? = null
    var luggage: Luggage? = null
    var payment: Payment? = null

}