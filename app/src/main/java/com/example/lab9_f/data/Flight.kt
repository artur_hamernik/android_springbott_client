package com.example.lab9_f.data

import com.example.lab9_f.data.Airline
import com.example.lab9_f.data.Airport
import java.io.Serializable

class Flight :Serializable{
    val id = 0
    val airline: Airline? = null
    val departure: String? = null
    val arrival: String? = null
    val capacity = 0
    val from: Airport? = null
    val to: Airport? = null
    val price = 0

}
