package com.example.lab9_f.data

import java.io.Serializable

class Passenger :Serializable{
    val id_customer = 0
    var name: String? = null
    var surname: String? = null
    var birth_date: String? = null
    var sex: String? = null
    var document: Document? = null
    var login: String? = null
    var password: String? = null

}
