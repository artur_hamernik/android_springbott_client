package com.example.lab9_f.data

import java.io.Serializable

class Document: Serializable {
    var document_serial: String? = null
    var document_type: String? = null
    var expiration_date: String? = null
    var citizenship: String? = null
}