package com.example.lab9_f.data

import java.io.Serializable

class Airport :Serializable {
    val name: String? = null
    val location: String? = null
    val timezone: String? = null

}