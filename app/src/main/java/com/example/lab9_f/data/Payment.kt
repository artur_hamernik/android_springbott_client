package com.example.lab9_f.data

import java.io.Serializable

class Payment :Serializable{
    var payment_type: String? = null
    var pay_value = 0
    var currency: String? = null
    var date: String? = null
    var status: String? = null

}